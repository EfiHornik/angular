// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyD1DPED4RzSRzGicuFLTmAUW46f7oL1pyk",
    authDomain: "hello-1e613.firebaseapp.com",
    databaseURL: "https://hello-1e613.firebaseio.com",
    projectId: "hello-1e613",
    storageBucket: "hello-1e613.appspot.com",
    messagingSenderId: "281377799824",
    appId: "1:281377799824:web:f11fca61ecad178264a215"
  }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
