import { Component, OnInit } from '@angular/core';
import { BooksService } from '../books.service';
import { AuthService } from './../auth.service';



@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  //books;
  books$;
  userId:string;
  
  panelOpenState = false;
  constructor(private booksService:BooksService , public authService:AuthService) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        console.log(this.userId);
        this.books$ = this.booksService.getBooks(this.userId);
      }
    )
  }


}

