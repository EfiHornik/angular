import { Posts } from './../interfaces/posts';
import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  panelOpenState = false;
  userId: number;
  id: number;
  title: string;
  body: string;
  postsData$:Observable<Posts> 
  hasError:boolean = false; 
  
  constructor(private PostsService:PostsService) { }

  ngOnInit(): void {
    this.postsData$ = this.PostsService.searchPostsData();
    this.postsData$.subscribe(
      data => {
                this.userId=data.userid;
                this.id= data.id;
                this.title=data.title;
                this.body= data.body;
              },
      error => {
        console.log(error.message);
        this.hasError=true;
      })
}


}

