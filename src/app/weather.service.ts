import { WeatherRaw } from './interfaces/weather-raw';
import { Weather } from './interfaces/weather';
import { getLocaleDayNames } from '@angular/common';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError, Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {
  private URL = "https://api.openweathermap.org/data/2.5/weather?q=";
  private KEY= "1971c339f776ebd2ede9b248bcb1c5cc";
  private IMP="units=meric";
  constructor(private http:HttpClient) { }

  searchWeaterData(cityName:string):Observable<Weather>{
    return this.http.get<WeatherRaw>(`${this.URL}${cityName}&APPID=${this.KEY}&${this.IMP}`).pipe(
      map(data => this.transformWeatherData(data)),
      catchError(this.handleError)
    )
  }

  private handleError(res:HttpErrorResponse){
    console.log(res.error);
    return throwError(res.error || 'Server error');

  }

  private transformWeatherData(data:WeatherRaw):Weather{
    return{
      name:data.name,
      country:data.sys.country,
      image:`https://home.openweathermap.org/img/w/${data.weather[0].icon}.png`,
      description:data.weather[0].description,
      temperature:data.main.temp,
      lat:data.coord.lat,
      lon:data.coord.lon
      
    }

  }
  
}
